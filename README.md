# The Ambient Cloud Project

Le cloud ambient est formé de multiples cellules, certaines autonomes, d'autres interdépendantes.

* Spécifier les variables dans group_vars/cell
* Remplir le fichier hosts pour spécifier l'adresse du serveur à gérer, le user et le chemin de la clé
* lancer :

```
ansible-playbook -i hosts seed.yml
```

* Se connecter sur la machine et, en root, lancer :

```
setup-kolab
```

* Suivre la procédure interactive du setup
